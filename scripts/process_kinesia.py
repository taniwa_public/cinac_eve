###############################################################################
# name        : process_movements
# Description : movement proccesor: Parkinson's Disease Test analyzer
# Authors     : Taniwa Solutions and CINAC (jlmarina@taniwa.es and aferro.hmcinac@hmhospitales.com )
# Contributors: CINAC Hospitales Sur  de Madrid
# Date        : 2019-July
#
#
# This script processes several files with movement information from Kinesia Devices.
# - Converts from Left Right to Dominant or Non-Dominant based on the subject (lefhanded of foot handed)
#    * See names_dominance.cfg
# - Then process each test file, extract features and generate a PDF page and a CSV file.
#
# Test names from Kinesia are (File name format is: 999-99-DDMMYYY-HHMISS-<TEST_NAME>.csv):
#     FingerTaps-L
#     FingerTaps-R
#     HandMovements-L
#     HandMovements-R
#     KineticTremor-L
#     KineticTremor-R
#     LegLifts-L
#     LegLifts-R
#     PosturalTremor-L
#     PosturalTremor-R
#     RapidAlternatingMovements-L
#     RapidAlternatingMovements-R
#     RestTremor-L
#     RestTremor-R
#     ToeTaps-L
#     ToeTaps-R
#
# ** To build and develop this we used conda (see the docker file in this repo):
#   conda config --add channels conda-forge
#   conda install mtspec
#
#
###############################################################################
# MIT License
# Copyright (c) 2019 CINAC Hospitales Sur de Madrd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
###############################################################################
import os 
import glob
import sys, getopt
import logging

import pandas as pd
import numpy as np
import scipy
import matplotlib.pyplot as plt
plt.rcParams.update({'figure.max_open_warning': 0})
import matplotlib.gridspec as gridspec

from scipy.signal import hilbert, periodogram
from scipy.stats import kurtosis, skew
from scipy.integrate import simps
from scipy.stats import linregress


from matplotlib.backends.backend_pdf import PdfPages

## Not used anymore:
#from cinac_tools import filter_fft_lowpass
#from cinac_tools import butter_highpass_filter
#from cinac_tools import get_autocorr_values
#from cinac_tools import autocorr

from cinac_tools import butter_lowpass
from cinac_tools import butter_lowpass_filter

from cinac_tools import detect_peaks
from cinac_tools import princomp
from cinac_tools import frequency_analysis_FFT

import mtspec

#######################################################################################################################################################
# Global things
#######################################################################################################################################################
TEST_CONFIG_FILE        = './tests.cfg' 
DOMINANCE_FILE          = './names_dominance.cfg'
PDF_FILENAME            = "/tmp/results.pdf"
CSV_FILENAME            = "/tmp/results.csv"
SECONDS_TO_DISCARD_INI  = 2
SECONDS_TO_PROCESS      =12
CUTOFF_FREQ_HZ          = 5.0 
DATA_DIR                = "/tmp/"
SHOW_CHARTS             = False
SHOW_PDF                = True
LEFT_FOOTED             = False
LEFT_HANDED             = False
USER_ID                 = 'userID00'
USAGE                   = 'process_kinesia.py -i <user_id> -d <data_directory> [-h is left handed] [-f is left footed]'
TABLE_COL_COLORS        = ["#aaaacf","w","w","w","w","w","w"]

FEATURE_NAMES = ['PeaksNumRot',   'SpeedRot', 'PeaksMeanRot', 'RotIntensity', 'PCAFreqMaxValue','Kurtosis']
FEATURE_DESCRIPTIONS = ['# Movements',   'Speed', 'Amplitude', 'Intensity', 'Max Frequency','Kurtosis']
FEATURE_NAMES_AVG = ['PeaksNumRotAvg',  'SpeedRotAvg', 'PeaksMeanRotAvg',  'RotIntensityAvg', 'PCAFreqMaxValueAvg','KurtosisAvg']
FEATURE_NAMES_STDEV = ['PeaksNumRotStdev',  'SpeedRotStdev', 'PeaksMeanRotStdev', 'RotIntensityStdev','PCAFreqMaxValueStdev','KurtosisStdev']

#######################################################################################################################################################
# Complete set of features:
# ========================
# FEATURE_NAMES = ['MeanPCA', 'MeanAcc', 'MeanRot', 'PeaksNumPCA', 'PeaksNumAcc', 'PeaksNumRot', 'SpeedPCA', 'SpeedAcc', 'SpeedRot', 
#                  'PeaksMeanPCA', 'PeaksStdPCA', 'PeaksMaxPCA', 'PeaksMeanAcc', 'PeaksStdAcc', 'PeaksMaxAcc', 'PeaksMeanRot', 'PeaksStdRot', 
#                  'PeaksMaxRot', 'PeaksSlopePCA', 'SlopePCA', 'PeaksSlopeAcc', 'SlopeAcc', 'PeaksSlopeRot', 'SlopeRot', 'PCAContribution', 
#                  'PCAMainFreqPSD', 'PCAFreqMaxValue', 'PCAIntensity', 'AccIntensity', 'RotIntensity', 'PeriodicityPSD', 'Skewness', 'Kurtosis']

# FEATURE_NAMES_AVG = ['MeanPCAAvg', 'MeanAccAvg', 'MeanRotAvg', 'PeaksNumPCAAvg', 'PeaksNumAccAvg', 'PeaksNumRotAvg', 'SpeedPCAAvg', 'SpeedAccAvg', 'SpeedRotAvg', 
#                  'PeaksMeanPCAAvg', 'PeaksStdPCAAvg', 'PeaksMaxPCAAvg', 'PeaksMeanAccAvg', 'PeaksStdAccAvg', 'PeaksMaxAccAvg', 'PeaksMeanRotAvg', 'PeaksStdRotAvg', 
#                  'PeaksMaxRotAvg', 'PeaksSlopePCAAvg', 'SlopePCAAvg', 'PeaksSlopeAccAvg', 'SlopeAccAvg', 'PeaksSlopeRotAvg', 'SlopeRotAvg', 'PCAContributionAvg', 
#                  'PCAMainFreqPSDAvg', 'PCAFreqMaxValueAvg', 'PCAIntensityAvg', 'AccIntensityAvg', 'RotIntensityAvg', 'PeriodicityPSDAvg', 'SkewnessAvg', 'KurtosisAvg']
# FEATURE_NAMES_STDEV = ['MeanPCAStdev', 'MeanAccStdev', 'MeanRotStdev', 'PeaksNumPCAStdev', 'PeaksNumAccStdev', 'PeaksNumRotStdev', 'SpeedPCAStdev', 'SpeedAccStdev', 'SpeedRotStdev', 
#                  'PeaksMeanPCAStdev', 'PeaksStdPCAStdev', 'PeaksMaxPCAStdev', 'PeaksMeanAccStdev', 'PeaksStdAccStdev', 'PeaksMaxAccStdev', 'PeaksMeanRotStdev', 'PeaksStdRotStdev', 
#                  'PeaksMaxRotStdev', 'PeaksSlopePCAStdev', 'SlopePCAStdev', 'PeaksSlopeAccStdev', 'SlopeAccStdev', 'PeaksSlopeRotStdev', 'SlopeRotStdev', 'PCAContributionStdev', 
#                  'PCAMainFreqPSDStdev', 'PCAFreqMaxValueStdev', 'PCAIntensityStdev', 'AccIntensityStdev', 'RotIntensityStdev', 'PeriodicityPSDStdev', 'SkewnessStdev', 'KurtosisStdev']
#######################################################################################################################################################

logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stderr)
logger = logging.getLogger("_EVE_")
logger.setLevel(logging.INFO)

#====================================================================================================
def change_file_to_dominant(names_dominance_file, tests_path,left_footed, left_handed):
    onlyfiles = [f for f in os.listdir(tests_path) if os.path.isfile(os.path.join(tests_path, f))]

    # for f in onlyfiles:
    #     logging.info("<<<<<< %s >>>>>>",f)

    # logging.info ("Applying dominace names to test files... LEFT_FOOTED?: %d LEFT_HANDED?: %d", left_footed, left_handed) 
    tests_df = pd.read_csv(names_dominance_file)

    # change_file_to_dominant(tests_path, left_footed, left_handed)

    for _, row in tests_df.iterrows():
        test_filenames = glob.iglob(tests_path + "*" + row['name'] + ".csv")
        for file_name in test_filenames:
            new_filename = file_name
            if row['upper_limbs'] :
                if left_handed :
                    new_filename = file_name.replace("-R.csv","-N.csv").replace("-L.csv","-D.csv")
                else:
                    new_filename = file_name.replace("-R.csv","-D.csv").replace("-L.csv","-N.csv")
            else:
                if left_footed :
                    new_filename = file_name.replace("-R.csv","-N.csv").replace("-L.csv","-D.csv")
                else:
                    new_filename = file_name.replace("-R.csv","-D.csv").replace("-L.csv","-N.csv")
            os.rename(file_name, new_filename)

#====================================================================================================
def load_tests_for_usersession(dominance_file,test_config_file, tests_path, user_id,left_footed, left_handed):
    
    logging.info ("Loading  Test...") 
    tests_df = pd.read_csv(test_config_file)
    tests_df['data_path'] = "NULL"
    tests_df['user_id'] = user_id
    
    change_file_to_dominant(dominance_file,  tests_path, left_footed, left_handed)

    for index, row in tests_df.iterrows():
        iter_filenames = glob.iglob(tests_path + "*" + row['name'] + ".csv")
        
        for file_name in iter_filenames:
            if row['data_path'] != "NULL":
                break
            tests_df.at[index,'data_path'] = file_name
    logging.info ("=> %d tests loaded",tests_df['name'].count())

    return tests_df

#==============================================================================

def load_test(csv_file, sampling_ms):
    """Load the expected CSV file and process some calculations:
        Principal Component Analysis
        Compute the Acceleration and Rotation norms.
        Cleans the unused columns in the Pandas array.
        Cuts the initial seconds of the timeseries.
    
    Args:
        argv: csv_file path
              sampling time in milliseconds.

    Returns:
        data: with columns => Time, AccMod, RotMod, MainPCA
        main_pca_contribution => Percentage of contribution.

    """
    
    try:
        dt1 = pd.read_csv(csv_file,  usecols=['Time','AccelX','AccelY','AccelZ','RotateX','RotateY','RotateZ'])
    except Exception as e:
        logging.error("Could not load test %s error(%s) Next!!!",csv_file,e)
        return [], 0.0
        # print ('{"code": "2","message":"Could not Load Test"',csv_file , ',"data":{}}',file=sys.stdout)
        # sys.exit(2)

    # Principal Component Analysis:
    # Get the first component ([0]) and its contribution.
    pca_data, _, eigen_values = princomp(dt1[['AccelX','AccelY','AccelZ','RotateX','RotateY','RotateZ']])
    main_pca_contrib = 100.0*eigen_values[0]/sum(eigen_values)
    # logging.info ("PCA[0] Contribution: [%0.2f]", main_pca_contrib)
    dt1['PCA'] = pca_data[0]

    # Norm of acceleration and rotation:
    dt1 ['Acc'] =  np.linalg.norm(dt1[['AccelX','AccelY','AccelZ']], axis=1)
    dt1 ['Rot'] =  np.linalg.norm(dt1[['RotateX','RotateY','RotateZ']], axis=1)
    dt1.drop(['AccelX','AccelY','AccelZ','RotateX','RotateY','RotateZ'], axis=1, inplace=True)
    # Delete first X seconds and take Y seconds after that.
    # dt1 = dt1.iloc[int(SECONDS_TO_DISCARD_INI*1000/sampling_ms):int((SECONDS_TO_DISCARD_INI+SECONDS_TO_PROCESS)*1000/sampling_ms)]

    dt1 = dt1[dt1['Time']> SECONDS_TO_DISCARD_INI ]
    dt1 = dt1[dt1['Time']< SECONDS_TO_DISCARD_INI + SECONDS_TO_PROCESS ]

    dt1.reset_index(drop=True, inplace=True)

    return dt1, main_pca_contrib

#==============================================================================
def check_args(argv):
    """Check correct arguments in the command line call.
    This processis prepared to called from an HTTP wrapper.

    Args:
        argv: Variable length argument list.

    Returns:
        data_dir: Directory where the data is.
        user_id: User ID
        show_charts: If we must generate charts.
        show_pdf: Shall we produce a PDF file?

    """
    data_dir = ''
    user_id=''
    show_charts = False
    show_pdf    = True   # Always True!!
    left_footed = False
    left_handed = False

    try:
        opts, args = getopt.getopt(argv,"hfsd:i:",["datadir="])
    except getopt.GetoptError:
        print (USAGE,file=sys.stderr)
        print ('{"code": "3","message":"Bad arguments in call","data":{}}',file=sys.stdout)
        sys.exit(3)
    for opt, arg in opts:
        if opt == '-s':
            show_charts=True
        elif opt in ("-d", "--datadir"):
            data_dir = arg
        elif opt in ("-i", "--userid"):
            user_id = arg
        elif opt == '-f':   
            left_footed = True  
        elif opt == '-h':
            left_handed = True         

    if (data_dir == '' or user_id == ''):
        print (USAGE,file=sys.stderr)
        print ('{"code": "3","message":"Bad arguments in call","data":{}}',file=sys.stdout)
        sys.exit(3)
    
    return data_dir, user_id, show_charts, show_pdf, left_footed, left_handed
#===========================================================================================================================================
#                                   MAIN
#===========================================================================================================================================
DATA_DIR, USER_ID, SHOW_CHARTS, SHOW_PDF, LEFT_FOOTED, LEFT_HANDED = check_args(sys.argv[1:])

results_dir = DATA_DIR + "results"
PDF_FILENAME = results_dir + "/" + USER_ID + ".pdf"
CSV_FILENAME = results_dir + "/" + USER_ID + ".csv"

tests = load_tests_for_usersession(DOMINANCE_FILE,  TEST_CONFIG_FILE, DATA_DIR, USER_ID, LEFT_FOOTED, LEFT_HANDED)

logging.info ("------------------------- BEGIN ----------------------------------------")
logging.info ("  UserID  is  [%s] LH: %d LF: %d",USER_ID, LEFT_HANDED, LEFT_FOOTED)
logging.info ("  DataDir is.................:[%s]",DATA_DIR)
logging.info ("  Dominance CFG File is......:[%s]",DOMINANCE_FILE)
logging.info ("  Tests Config File is.......:[%s]",TEST_CONFIG_FILE)
logging.info ("------------------------------------------------------------------------")

if not os.path.exists(results_dir):
    os.makedirs(results_dir)

if SHOW_PDF:
    with PdfPages(PDF_FILENAME) as pdf:
# Process each test
        for index, row in tests.iterrows():    
            
            sampling_time = row['sampling_ms']
            lowpass_cut   = row['lowpass_cut']
            logging.info ("Tests %d: %s  Sampling(%d) LowPass: (%lf) ",index+1, row['name'], sampling_time,lowpass_cut)

            # Load and clean data from the test:
            test_df, first_pca_contrib = load_test(row['data_path'],sampling_time)
            if first_pca_contrib <= 0:
                continue

            # Filtering using Butter
            fs = 1000.0/sampling_time # sample rate, Hz
            order = 5
            test_df['AccFilt']  = butter_lowpass_filter(test_df['Acc'], lowpass_cut, fs, order)
            test_df['RotFilt']  = butter_lowpass_filter(test_df['Rot'], lowpass_cut, fs, order)
            test_df['PCAFilt']  = butter_lowpass_filter(test_df['PCA'], lowpass_cut, fs, order)

            total_time    = (sampling_time * test_df['Time'].size / 1000)

            # -------------------------------------------------------------------------
            # Test function to see if  the rest makes sense Not used anymore.
            # t = test_df['Time']
            # test_df['PCAFilt']  = 5*np.sin(2*np.pi*4*t) + np.sin(2*np.pi*7*t) + np.random.randn(len(t))*0.2 
            # -------------------------------------------------------------------------

            # -------------------------------------------------------------------------
            #  Frequencies:  Power Spectral Density (PSD):
            # -------------------------------------------------------------------------
            freqPSD, specPSD =  periodogram(test_df['PCAFilt'], fs, 'flattop', scaling='spectrum')

            mainfreqPSD = freqPSD[specPSD >= max(specPSD)]
            idx_band = np.logical_and(freqPSD >= mainfreqPSD -.3, freqPSD <= mainfreqPSD + .3)
            
            bp = simps(specPSD[idx_band], dx=fs)
            bp /= simps(specPSD, dx=fs)
            # -------------------------------------------------------------------------
            # Peaks
            # -------------------------------------------------------------------------
            pca_peaks, pca_y = detect_peaks(test_df['PCAFilt'], mpd=4, mph = 0.8 * test_df['PCAFilt'].mean(),show=False)
            acc_peaks, acc_y = detect_peaks(test_df['AccFilt'], mpd=4, mph = 0.9 * test_df['AccFilt'].mean(),show=False)
            rot_peaks, rot_y = detect_peaks(test_df['RotFilt'], mpd=4, mph = 0.8 * test_df['RotFilt'].mean(),show=False)
            # -------------------------------------------------------------------------
            # Slopes of lines from regression:
            # -------------------------------------------------------------------------
            slope = pd.DataFrame(index=[0], columns=['pca','acc','rot']).fillna(0.0)
            peaks_slope = pd.DataFrame( index=[0],columns=['pca','acc','rot']).fillna(0.0)
            
            if test_df['Time'].size > 2:
                slope['pca'], _, _, _, _ = linregress(test_df['Time'], test_df['PCAFilt'])
                slope['acc'], _, _, _, _ = linregress(test_df['Time'], test_df['AccFilt'])
                slope['rot'], _, _, _, _ = linregress(test_df['Time'], test_df['RotFilt'])
            if pca_peaks.size > 2:
                peaks_slope['pca'], _, _, _, _ = linregress(pca_peaks, pca_y[pca_peaks])
            if acc_peaks.size > 2:    
                peaks_slope['acc'], _, _, _, _ = linregress(acc_peaks, acc_y[acc_peaks])
            if rot_peaks.size > 2:    
                peaks_slope['rot'], _, _, _, _ = linregress(rot_peaks, rot_y[rot_peaks])
            # -------------------------------------------------------------------------
            # ====== COMPUTE FEATURES ========
            # -------------------------------------------------------------------------
            tests.at[index,'MeanPCA']             = np.nanmean(test_df['PCAFilt'])
            tests.at[index,'MeanAcc']             = np.nanmean(test_df['AccFilt'])
            tests.at[index,'MeanRot']             = np.nanmean(test_df['RotFilt'])
            tests.at[index,'PeaksNumPCA']         = pca_peaks.size
            tests.at[index,'PeaksNumAcc']         = acc_peaks.size
            tests.at[index,'PeaksNumRot']         = rot_peaks.size
            tests.at[index,'SpeedPCA']            = pca_peaks.size / total_time
            tests.at[index,'SpeedAcc']            = acc_peaks.size / total_time
            tests.at[index,'SpeedRot']            = rot_peaks.size / total_time
            
            if pca_peaks.size > 0:
                tests.at[index,'PeaksMeanPCA']         = np.nanmean(pca_y[pca_peaks])
                tests.at[index,'PeaksStdPCA']          = np.nanstd(pca_y[pca_peaks])
                tests.at[index,'PeaksMaxPCA']          = np.nanmax(pca_y[pca_peaks])
            else:
                tests.at[index,'PeaksMeanPCA']         = 0.0
                tests.at[index,'PeaksStdPCA']          = 0.0
                tests.at[index,'PeaksMaxPCA']          = 0.0 
            if acc_peaks.size > 0:
                tests.at[index,'PeaksMeanAcc']         = np.nanmean(acc_y[acc_peaks])
                tests.at[index,'PeaksStdAcc']          = np.nanstd(acc_y[acc_peaks])
                tests.at[index,'PeaksMaxAcc']          = np.nanmax(acc_y[acc_peaks])
            else:
                tests.at[index,'PeaksMeanAcc']         = 0.0
                tests.at[index,'PeaksStdAcc']          = 0.0
                tests.at[index,'PeaksMaxAcc']          = 0.0 
            if rot_peaks.size > 0:
                tests.at[index,'PeaksMeanRot']         = np.nanmean(rot_y[rot_peaks])
                tests.at[index,'PeaksStdRot']          = np.nanstd(rot_y[rot_peaks])
                tests.at[index,'PeaksMaxRot']          = np.nanmax(rot_y[rot_peaks])
            else:
                tests.at[index,'PeaksMeanRot']         = 0.0
                tests.at[index,'PeaksStdRot']          = 0.0
                tests.at[index,'PeaksMaxRot']          = 0.0                  
            tests.at[index,'PeaksSlopePCA']        = peaks_slope.at[0,'pca']
            tests.at[index,'SlopePCA']             = slope.at[0,'pca']
            tests.at[index,'PeaksSlopeAcc']        = peaks_slope.at[0,'acc']
            tests.at[index,'SlopeAcc']             = slope.at[0,'acc']
            tests.at[index,'PeaksSlopeRot']        = peaks_slope.at[0,'rot']
            tests.at[index,'SlopeRot']             = slope.at[0,'rot']
            tests.at[index,'PCAContribution']   = first_pca_contrib
            tests.at[index,'PCAMainFreqPSD']    = freqPSD[specPSD >= max(specPSD)]
            tests.at[index,'PCAFreqMaxValue']   = max(specPSD)
            tests.at[index,'PCAIntensity']      = np.sqrt(np.mean(test_df['PCAFilt']**2))
            tests.at[index,'AccIntensity']      = np.sqrt(np.mean(test_df['AccFilt']**2))
            tests.at[index,'RotIntensity']      = np.sqrt(np.mean(test_df['RotFilt']**2))
            tests.at[index,'PeriodicityPSD']    = bp
            tests.at[index,'Skewness']          = skew(specPSD)
            tests.at[index,'Kurtosis']          = kurtosis(specPSD)
            #print(tests.loc[index][FEATURE_NAMES].to_string())
            # ========================== END FEATURES ===============================================
            fval = tests.loc[index][FEATURE_NAMES]
            favg = tests.loc[index][FEATURE_NAMES_AVG]
            fstdev = tests.loc[index][FEATURE_NAMES_STDEV]
            # print(fval)
            # print(favg)
            t1 = pd.DataFrame(tests.loc[index][FEATURE_NAMES]).transpose().applymap("{0:,.2f}".format)
            t2 = pd.DataFrame(tests.loc[index][FEATURE_NAMES_AVG]).transpose().applymap("{0:,.2f}".format)
            t2.columns = FEATURE_NAMES
            t3 = pd.DataFrame(tests.loc[index][FEATURE_NAMES_STDEV]).transpose().applymap("{0:,.2f}".format)
            t3.columns = FEATURE_NAMES
            t3 = t1.append(t2,ignore_index=True).append(t3, ignore_index = True)
            t3['Feature'] = ['Value', 'Control Avg', 'Control StDev']
            cols = t3.columns.tolist()
            cols = cols[-1:] + cols[:-1]
            t3 = t3[cols]
            
            zscore =(fval.values - favg.values)/ fstdev.values
            ft = np.arange(1,len(zscore)+1)
            
            # Always True!! Always generate PDF for the end user.
            if SHOW_PDF :
                plt.style.use("seaborn-whitegrid") # "ggplot" "seaborn-whitegrid" "fivethirtyeight" "Solarize_Light2"
                # JLM f, xarr = plt.subplots(4, 1)
                f = plt.figure() # tight_layout=True
                gs = gridspec.GridSpec(4, 6)

                # ==============================================================
                # Graph-1: Z-Score of main features
                # =============================================================
                
                f_ax0 = f.add_subplot(gs[0, :])
                f_ax0.set_title("Z-score",fontsize=12)
                f_ax0.xaxis.set_major_locator(plt.MaxNLocator(len(ft)))
                f_ax0.axes.set_xticklabels([])
                # f_ax0.axes.set_yticklabels([])
                f_ax0.axes.grid(axis='x', linestyle="-", color='grey')
                f_ax0.set_ylim([-3.0,3.0])

                f_ax0.plot(ft,zscore,'--ro',)
                f_ax0.errorbar(ft, np.zeros(len(zscore)), yerr = np.ones(len(zscore)), elinewidth=20, fmt='', color = '#b2d2d6', alpha = 0.5,  ls = 'none', lw = 2,  capthick = 5) 

                #f_ax0.scatter(ft,zscore,c='r')
                for i, feature in enumerate(FEATURE_NAMES):
                    f_ax0.text(ft[i]+0.03,zscore[i],FEATURE_DESCRIPTIONS[i])
                # Draw error bars to show standard deviation, set ls to 'none'
                # to remove line between points
                
                # ==============================================================
                # Graph-3: Table data
                # ==============================================================
                f_ax1 = f.add_subplot(gs[1, :])
                f_ax1.axis('off')
                f_ax1.axis('tight')
                clabels = ['Features'] + FEATURE_DESCRIPTIONS
                f_ax1.table(cellText=t3.values, colLabels=clabels, loc='center', colColours = TABLE_COL_COLORS)

                # ==============================================================
                # Graph-2: Rotation
                # ==============================================================
                f_ax2 = f.add_subplot(gs[2, :])
                f_ax2.set_title("Signal Rotation Module",fontsize=12)
                f_ax2.set_xlabel("Intensity:"+ "{:.3f}".format(tests.at[index,'RotIntensity']))
                f_ax2.plot(test_df[['RotFilt']],'r')
                f_ax2.plot(rot_peaks, rot_y[rot_peaks],'+', mfc=None, mec='b', mew=2, ms=8)

                # ==============================================================
                # Graph-3: Freq
                # ==============================================================
                f_ax3 = f.add_subplot(gs[3, :])
                f_ax3.set_title("Frequency",fontsize=12)
                f_ax3.set_xlabel("Main Freq:" + "{:.2f}".format(tests.at[index,'PCAMainFreqPSD']) + 
                    " Intensity:"+ "{:.2f}".format(tests.at[index,'PeriodicityPSD']))
                f_ax3.plot(freqPSD, specPSD,'r')

                # ==============================================================
                plt.tight_layout()
                plt.suptitle(row['name'] + " " + USER_ID + " [" + ('Left-H' if LEFT_HANDED else 'Rigth-H') + "|" + ('Left-F' if LEFT_FOOTED else 'Rigth-F')+"]", fontsize=16)
                plt.subplots_adjust(top=0.90, hspace=1.1)
                d = pdf.infodict()
                d['Title'] = 'CINAC EVE Movement Analysis'
                d['Author'] = 'hola@taniwa.es'
                N = 2
                params = plt.gcf()
                plSize = params.get_size_inches()
                params.set_size_inches( (plSize[0]*N, plSize[1]*N) )
                pdf.savefig()
                pdf.close

# -------------------------------------------------------------------------
# print (tests) #
tests.to_csv(CSV_FILENAME)
print ('{"code": "0","message":"OK","data":{"csv":"' + os.path.abspath(CSV_FILENAME) + '","pdf":"' + os.path.abspath(PDF_FILENAME) + '"}}',file=sys.stdout)
logging.info ("-------------------------  BYE !! ---------------------------------")