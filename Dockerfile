#######################################################################
#            Laravel/Lumen 5.7 Application - Dockerfile v0.5          #
#######################################################################

# registry.gitlab.com/taniwa_public/cinac_eve

#------------- Setup Environment -------------------------------------------------------------

# Pull base image
FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

# Install common tools 
RUN apt-get update
RUN apt-get install -y wget curl nano htop git unzip bzip2 software-properties-common locales

# Set evn var to enable xterm terminal
ENV TERM=xterm

# Set working directory
WORKDIR /var/www/html

# Set up locales 
# RUN locale-gen 

#------------- Application Specific Stuff ----------------------------------------------------

# Install PHP
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt update
RUN apt-get install -y \
    php7.2-fpm \ 
    php7.2-common \ 
    php7.2-curl \ 
    php7.2-mysql \ 
    php7.2-mbstring \ 
    php7.2-json \
    php7.2-xml \
    php7.2-bcmath

# Install NPM and Node.js
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs 

#------------- FPM & Nginx configuration ----------------------------------------------------

# Config fpm to use TCP instead of unix socket
ADD resources/www.conf /etc/php/7.2/fpm/pool.d/www.conf
RUN mkdir -p /var/run/php

# Install Nginx
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C
RUN echo "deb http://nginx.org/packages/ubuntu/ trusty nginx" >> /etc/apt/sources.list
RUN echo "deb-src http://nginx.org/packages/ubuntu/ trusty nginx" >> /etc/apt/sources.list
RUN apt-get update

RUN apt-get install -y nginx

ADD resources/default /etc/nginx/sites-enabled/
ADD resources/nginx.conf /etc/nginx/

#------------- Composer & laravel configuration ----------------------------------------------------

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#------------- Python Conda Config ---------------------------------------------------------------
# Set environment variables
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update --fix-missing && apt-get install -y wget bzip2 ca-certificates \
    libglib2.0-0 libxext6 libsm6 libxrender1 \
    git mercurial subversion

RUN wget --quiet https://repo.anaconda.com/archive/Anaconda3-2019.07-Linux-x86_64.sh -O ~/anaconda.sh && \
    /bin/bash ~/anaconda.sh -b -p /opt/conda && \
    rm ~/anaconda.sh && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

RUN conda config --add channels conda-forge
RUN conda install mtspec

#------------- Laravel Config ---------------------------------------------------------------

COPY ./cinaceveweb/ /var/www/html
COPY ./cinaceveweb/.env.CINACEVEWEB /var/www/html/.env

RUN mkdir -p /var/www/html/storage/app/tests
RUN mkdir -p /var/www/html/storage/app/bin
RUN mkdir -p /var/www/html/storage/app/public

COPY ./scripts/* /var/www/html/storage/app/bin/
COPY ./sample_output_soma13.pdf /var/www/html/storage/	

RUN chown -R www-data:www-data /var/www/html

RUN ln -s /var/www/html/storage /var/www/html/public/storage

RUN chown -R www-data:www-data /var/www/html/public

RUN chmod -R 777 /var/www/html/storage && \
  chmod -R 777 /var/www/html/bootstrap/cache

RUN cd /var/www/html && composer install

WORKDIR /var/www/html

#------------- Supervisor Process Manager ----------------------------------------------------

# Install supervisor
RUN apt-get install -y supervisor
RUN mkdir -p /var/log/supervisor
ADD resources/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Expose port 80
EXPOSE 80

ADD entrypoint.sh /entrypoint.sh
 
RUN chmod +x /entrypoint.sh

# Set supervisor to manage container processes
ENTRYPOINT ["/entrypoint.sh"]
