#!/bin/bash
USERDATADIR='../data/rawdata'

#python3 cinac_process_kinesia_files.py -i soma123 -d ../data/rawdata/soma13/

for user in $( ls -1 ${USERDATADIR} ); do
	python3 process_PCA.py -i ${user} -p -d "${USERDATADIR}/${user}/"
done

cat ${USERDATADIR}/soma*/results/results.csv > ${USERDATADIR}/all_results.csv

