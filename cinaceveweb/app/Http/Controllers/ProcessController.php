<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Storage;

use GuzzleHttp\Client;

use Log;

use Symfony\Component\Process\Process;

class ProcessController extends Controller
{
    /**
     * Handle the incoming request.
     *
		 * prueba
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request)
    {
        //
        try {

            $validator = Validator::make($request->all(), [
                'file' => 'required|mimetypes:application/zip|max:5120',
                'userid' => 'required|string|max:35',
                'lefthand' => 'nullable|string',
                'leftfoot' => 'nullable|string'
            ])->validate();

            // Data
            $uploadedFile = $request->file('file');
            $userid = str_replace(' ', '',$request->input('userid'));
            if (""===$userid) {
                $userid = "RESULTS";
            }
            $lefthand = $request->input('lefthand');
            $leftfoot = $request->input('leftfoot');
            Log::debug("userid: ".$userid);
            Log::debug("lefthand: ".$lefthand);
            Log::debug("leftfoot: ".$leftfoot);

            $file_parts = pathinfo($uploadedFile->getClientOriginalName());
            $filename = $file_parts['filename'];
            $uuid = uniqid();
            $resultname =  $userid.'_'.$uuid;

            Log::debug("Result Name: ".$resultname);

            $file = 'tests/'.$resultname.'.zip';
            Log::debug("File: ".$file);

            $filePath = storage_path().'/app/'.$file;
            Log::debug("File Path: ".$filePath);

            Storage::put($file, file_get_contents($uploadedFile));

            // unzip directory > uuid from zip file
            $directory = 'tests/'.$uuid;
            Log::debug("Directory: ".$directory);

            $directoryPath = storage_path().'/app/'.$directory;
            Log::debug("Directory Path: ".$directoryPath);

            Storage::makeDirectory($directory);

            //\Zipper::make($filePath)->extractTo($directoryPath);

            $exec1="cd ".$directoryPath." && unzip -qq ".$filePath." && ";
            $exec=$exec1."cd ".storage_path()."/app/"."bin"." && "."PATH=/opt/conda/bin:/opt/conda/condabin python process_kinesia.py -i ".$userid." -d ".$directoryPath.'/';

            if ("on"===$lefthand) {
                $exec = $exec." -h";
            }

            if ("on"===$leftfoot) {
                $exec = $exec." -f";
            }

            Log::debug("exec: ".$exec);

            $process = new Process($exec);
            $process->run();

            // executes after the command finishes
            if (!$process->isSuccessful()) {
                Log::error($process->getErrorOutput());
                //Storage::deleteDirectory($directory);
                return redirect('/')->withErrors(['Internal problems. Please, Try later.']);
            }

            $result = json_decode($process->getOutput(),true);
            Log::debug('Output: '.$process->getOutput());

            if ((JSON_ERROR_NONE != json_last_error()) && (JSON_ERROR_CTRL_CHAR != json_last_error())) {
                Log::error('Error: '.$process->getErrorOutput());
                Log::error("process json format problems ".$result." ".json_last_error());
                return redirect('/')->withErrors(['Internal problems. Please, Try later.']);
            }

            if ("0"!=$result['code']) {
                Log::debug('Error: '.$process->getErrorOutput());
                Log::error("Result: ".$result['message']);
                return redirect('/')->withErrors(['Internal problems. Please, Try later.']);
            }

            $resultFilePdf =  $userid.'_'.$uuid.'.pdf';
            $resultFileCsv =  $userid.'_'.$uuid.'.csv';

            $reportPath = $directory.'/results/';

            Storage::copy($reportPath.'/'.$userid.'.pdf',$resultFilePdf);
            Storage::copy($reportPath.'/'.$userid.'.csv',$resultFileCsv);

            // remove in background

            $exec_remove = "sleep 300 && "."rm -rf ".$directoryPath." && "."rm ".$filePath." && ".
                "rm ".storage_path().'/app/'.$resultFilePdf." && "."rm ".storage_path().'/app/'.$resultFileCsv." & ";

            Log::debug('exec_remove: '.$exec_remove);

            $process_remove = new Process($exec_remove);
            $process_remove->start();

            return view('results',['name' => $resultname, 'pdffile' => Storage::url('app/'.$resultFilePdf),
                'csvfile' => Storage::url('app/'.$resultFileCsv)]);
        }
        catch (Exception $e) {
            return redirect('/')->withErrors(['Internal problems. Please, Try later.']);
        }
    }
}
