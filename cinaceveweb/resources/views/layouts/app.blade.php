<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>cinac eve: Movment Signals Analisys Tool</title>

	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/ladda-themeless.min.css">
    <script src="js/spin.min.js"></script>
    <script src="js/ladda.min.js"></script>

	<style>
		body {
			font-family: 'Raleway';
			margin-top: 25px;
		}

		.fa-btn {
			margin-right: 6px;
		}

		.table-text div {
			padding-top: 6px;
		}

		.footer {
		  position: relative;
		  margin-top: -150px; /* negative value of footer height */
		  height: 300px;
		  clear:both;
		  padding-top:20px;
		}

		#tagcloud {
    		color:#AA66FF;
    		padding: 10px;
        }

        #tagcloud a:link, #tagcloud a:visited {
                text-decoration:none;
                color: #033;
        }

        #tagcloud a:hover {
                text-decoration: underline;
        }

        #tagcloud span {
                padding: 4px;
        }

        #tagcloud .cloud-10 {
                font-size: 110%;
        }
        #tagcloud .cloud-20 {
                font-size: 120%;
        }
        #tagcloud .cloud-30 {
                font-size: 130%;
        }
        #tagcloud .cloud-40 {
                font-size: 140%;
        }
        #tagcloud .cloud-50 {
                font-size: 150%;
        }
        #tagcloud .cloud-60 {
                font-size: 160%;
        }
        #tagcloud .cloud-70 {
                font-size: 170%;
        }
        #tagcloud .cloud-80 {
                font-size: 180%;
        }
        #tagcloud .cloud-90 {
                font-size: 190%;
        }
        #tagcloud .cloud-100 {
                font-size: 200%;
        }

	</style>

</head>

<body>
	<div class="container">
		<nav class="navbar navbar-primary">
			<div class="container-fluid">
				<div class="navbar-header">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a class="navbar-brand" href="/" ><i class="glyphicon glyphicon-home"></i>      Cinac Eve</a></li>
                    </ul>
                </div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="https://www.hmcinac.com/"><img src="{{ asset('/images/hm-cinac.png') }}"> </i></a></li>
					</ul>
				</div>
			</div>
		</nav>
		@include('flash')
		@include('common.errors')
	</div>

	@yield('content')
</body>
<footer class="footer"></footer>
</html>
