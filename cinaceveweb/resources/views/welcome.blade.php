@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default panel-primary">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-flash" aria-hidden="true"></span> NEUROMOTOR DATA ANALYSIS
				</div>
				<div class="panel-body">
				      <div class="jumbotron">
				      	<h2>What is Cinac Eve?</h2>
                        <p>EVE is an open initiative from <strong>Centro Integral de Neuro-Ciencias CINAC</strong>
                            to provide an initial tool to process signal data from Kinesia ONE devices.</p>
                        <p>The patients perform different tests and the data gathered is compressed in a ZIP file, uploaded, analyzed
                            and compared to the control reference inside EVE.</p>
                        <p> The system resturns a <a target="_blank" rel="noopener noreferrer" href='sample_output_soma13.pdf'> PDF file </a>
                            and a <a href='sample_output_soma13.csv' download> CSV file</a> with the analysis.
                            An example of the ZIP input file can be found <a href='sample_input.zip' download>here</a></p>
                        <p> ** CINAC Eve does not store data or files in the cloud in anyway. Every datafile will be removed after you download your results files (* spanish: CINAC Eve no graba datos ni ficheros en ningún caso en la nube.
                            Todo fichero de datos se borran después de que el usuario descarga los resultados).</p>

                        <br>
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3">
                                    <form name="process" action="process" method="post" enctype="multipart/form-data">
                                        <div class="form-group text-center">
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <input type="text" maxlength="35" name="userid" class="form-control text-center" placeholder="Patient anonymous id">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 col-lg-12"><label class="input-lg" style="font-weight: normal"><input name="lefthand" type="checkbox"> is left handed? </label>
                                                <label class="input-lg" style="font-weight: normal"><input name="leftfoot" type="checkbox"> is left footed? </label></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <input type="file" class="form-control-file input-lg" name="file" id="file" style="height: auto"/>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                {{-- <button type="submit" id="btn-one" data-loading-text="Processing ..." class="btn btn-primary btn-lg" autocomplete="off"> Process</button> --}}
                                                <button type="submit" id="btn-one" class="btn btn-primary ladda-button" data-style="expand-left"><span class="ladda-label">Process</span></button>
                                            </div>

                                            {{ csrf_field() }}
                                            <br>
                                        </div>
                                    </form>
                                </div><!-- /.col-lg-4 -->
                            </div><!-- /.row -->
					  </div>
                          <h3 class="text-center">Find more about the project in </h3>
                          <h4 class="text-center"><a target="_blank" rel="noopener noreferrer" href='https://bitbucket.org/jlmarina/cinac_eve'>https://bitbucket.org/jlmarina/cinac_eve</a></h4>
				        <br>
				</div>
				<div class="panel-footer">Cinac Eve has been developed by <a href="http://www.taniwa.es" title="Taniwa Solutions">Taniwa Solutions</a> | Contact us here: <strong>admin@taniwa.es</strong></div>
			</div>
		</div>
	</div>

    <script>
        $( document ).ready(function() {

            Ladda.bind( '#btn-one' );
        });
    </script>

    {{-- <script>
            $('#btn-one').on('click', function()
            {
                var $this = $(this)
                $this.button('loading');
            });
    </script> --}}

@endsection

