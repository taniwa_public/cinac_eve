@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default panel-primary">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-flash" aria-hidden="true"></span> FAST PARKINSON SIGNAL PROCESSING
				</div>
				<div class="panel-body">
				      <div class="jumbotron">
                        <br>
                        <div class="row">
                                <h3 class="text-center">{{$name}}</h3>
                                <br>
                                <p> You have 5 minutes to download these files </p>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-4">
                                <div class="btn-group btn-group-justified" role="group">
                                    <a target="_blank" rel="noopener noreferrer" href="{{$pdffile}}" type="button" class="btn btn-primary btn-lg">
                                        <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> PDF
                                    </a>
                                    <a href="{{$csvfile}}" download type="button" class="btn btn-primary btn-lg">
                                        <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> CSV
                                    </a>
                                </div>
                            </div><!-- /.col-lg-4 -->
                        </div><!-- /.row -->
					  </div>
				</div>
				<div class="panel-footer">Cinac Eve has been developed by <a href="http://www.taniwa.es" title="Taniwa Solutions">Taniwa Solutions</a> | Contact us here: <strong>admin@taniwa.es</strong></div>
			</div>
		</div>
	</div>
@endsection
