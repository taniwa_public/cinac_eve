# CINAC Eve

Process movement data from neuromotor tests on Kinesia ONE devices to estimate Parkinson's Disease in patients.

![Image](sample_image.png)

## Goals:
1) Develop a Minimum Viable Product to help in the diagnosys of Parkinson's disease.
2) Open Component Based solution
3) Prepared for growth and scalability.

## Methodology:
- Agile and iterative.
- Find errors fast.
- Learn from the process.

## Getting Started
Goto: http://cinaceveweb.taniwa.es (WORKING ON IT)
And upload a ZIP file with all the data in the home page.

## Monitor UpTime
https://stats.uptimerobot.com/3lVPxTpo5B


## Got issues with the product?
* Goto https://bitbucket.org/jlmarina/cinac_eve/issues
* DIY! This is open-code.

## Input Files:
For each of the different tests:
One CSV file with 7 columns:
- Time (seconds, up to 9 decimals. 1.123456789) - Data is ordered by this column.
- AccelX, AccelY, AccelZ (Acceleration)
- RotateX, RotateY, RotateZ (Rotation)

## Output Files:
PDF File in: $DATA_DIR/results/$USERID.pdf
- Z-Score with the main features compared to the norm (mean and stdev)
- Data table
- Rotational amplitude filtered chart.
- Frequency domain chart.

CSV File in: $DATA_DIR/results/$USERID.csv
- All the features and control data.

## Available tests are:
    FingerTaps-L
    FingerTaps-R
    HandMovements-L
    HandMovements-R
    KineticTremor-L
    KineticTremor-R
    LegLifts-L
    LegLifts-R
    PosturalTremor-L
    PosturalTremor-R
    RapidAlternatingMovements-L
    RapidAlternatingMovements-R
    RestTremor-L
    RestTremor-R
    ToeTaps-L
    ToeTaps-R

File name format is: 999-99-DDMMYYY-HHMISS-<TEST_NAME>.csv

Customizable parameters for the whole process:
- The number of tests to upload is variable from 1 to 16 of the aforementioned (no test repetitions allowed in the same process)
- Sampling time in each test (default: 31 milliseconds)
- Butter LowPassFilter frequencies for each kind of test (if -1 no filter applies).
- Remove first N seconds in each test.

## Parameters from the user (asked in Web Page):
- Dominant Hand: Left or Right (default)
- Dominant Foot: Left or Right (default)
- UserID
- ZIP File with Kinesia tests data.

## Preprocessing
PCA applied to the 6 time series (3 acc + 3 rot) + LowPass Filtering.
AccMod + Filtering
RotMod + Filtering

## Features
For each of the preprocessed signals:
- Intensity: root-mean-square value of the signal.    => PCAIntensity
- Rate of movement: dominant frequency component.     => FFT and Multitappering tested but it seems that PSD works better.

Params in Multi taper:
   Frequencies:  Multi tapering
   Calculate the spectral estimation.
   specMT, freqMT = mtspec.mtspec(
           data=test_df['PCAFilt'], delta=sampling_time/1000, time_bandwidth=3,
           number_of_tapers=5, nfft=512, statistics=False)
Link with doc: https://krischer.github.io/mtspec/multitaper_mtspec.html

For PSD we are using Periodogram approach.
The function is this one: https://docs.scipy.org/doc/scipy-0.13.0/reference/generated/scipy.signal.periodogram.html
Call within the code: 
    freqPSD, specPSD =  periodogram(test_df['PCAFilt'], fs, 'flattop', scaling='spectrum')
 

- Periodicity: ratio of energy in the dominant frequency component to the total energy.
- Amplitude: Peaks Mean, Peaks Std, Mean and Std.
- Speed. Number of “taps” per second.
- Fatigue Slope of linear regression (on the complete the signal and peaks)
- Skewness: The skewness is a parameter to measure the symmetry of a data set 
A time series with an equal number of large and small amplitude values has a skewness of zero. A time series with many small values and few large values is positively skewed (right tail), and the skewness value is positive. A time series with many large values and few small values is negatively skewed (left tail), and the skewness value is negative.
- Kurtosis: The kurtosis to measure how heavy its tails are compared to a normal distribution.
Kurtosis measures the peakedness of the PDF - probability density function- of a time series.

## Feature Names:
    FEATURE_NAMES = ['MeanPCA', 'MeanAcc', 'MeanRot', 'PeaksNumPCA', 'PeaksNumAcc', 'PeaksNumRot', 'SpeedPCA', 'SpeedAcc', 'SpeedRot',
        'PeaksMeanPCA', 'PeaksStdPCA', 'PeaksMaxPCA', 'PeaksMeanAcc', 'PeaksStdAcc', 'PeaksMaxAcc', 'PeaksMeanRot', 'PeaksStdRot',
        'PeaksMaxRot', 'PeaksSlopePCA', 'SlopePCA', 'PeaksSlopeAcc', 'SlopeAcc', 'PeaksSlopeRot', 'SlopeRot', 'PCAContribution',
        'PCAMainFreqPSD', 'PCAFreqMaxValue', 'PCAIntensity', 'AccIntensity', 'RotIntensity', 'PeriodicityPSD', 'Skewness', 'Kurtosis']


### Prerequisites
You need:
* Datafiles with all the different tests
* User ID (non personal ID for your internal use)
* Right or Left dominant side for both hands and feet.


# Installing

CINAC Eve has been developed in php [laravel 5.7 framework](https://laravel.com) and python. 

The software is in the **cinaceveweb folder**.

For deploy it in your server you you have to follow the next steps (it has been tested in ubuntu 18.04)

* Install nginx (https://www.nginx.com/)
* Install laravel (https://laravel.com/docs/5.7/installation)
* Configure laravel for nginx (https://laravel.com/docs/5.7/deployment)
* Install Anaconda (https://www.anaconda.com/) 
* Install Anaconda mtspec package (https://anaconda.org/conda-forge/mtspec)
* Install unzip software (https://www.hostinger.com/tutorials/how-to-unzip-files-linux/)

## Contributing

Send an email to hola@taniwa.es or do it directly.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

# Authors

## Medical Support and Design:
* **Centro Integral de Neurociencias**  [CINAC](https://www.hmcinac.com/)
* PhD and MD Alvaro Sánchez 
* MD Mariana Hernández González-Monje

## Product Design, data processing and devops:
* **taniwa solutions** -  [Taniwa](http://taniwa.es)
* Joselu Marina (jlmarina@taniwa.es)
* Jesús Pancorbo (jpancorb@taniwa.es)

# License

This project is licensed under the MIT License [LICENSE]
MIT License
Copyright (c) 2019 CINAC Hospitales Sur de Madrd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


