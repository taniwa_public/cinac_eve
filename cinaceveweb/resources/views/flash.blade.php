@if (session()->has('message'))
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="alert alert-info">{{ session('message') }}</div>
				</div>
			</div>
		</div>
	</div>
@endif